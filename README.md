# TP : Simulation d'une Attaque ARP Spoofing avec Docker

## Exercice 1

### Introduction

1. **Allez sur vdn**

2. **Chargez le réseau docker**

3. **Démarrez la machine et ouvrez un terminal**


### Partie 1 : Mise en place de l'environnement


1. **Clonez le repository git**

    ```bash
    git clone https://codefirst.iut.uca.fr/git/adam.bonafos/TP_ARP-Spoofing.git
    ```

1. **Vérifiez le contenu du fichier docker-compose.yml**

   Le fichier `docker-compose.yml` sert à simuler un réseau local avec trois machines : un attaquant, une victime, et une passerelle (gateway).

   **Fichier `docker-compose.yml` :**
   
   ```yaml
   version: '3'
   services:
     attacker:
       image: kalilinux/kali-rolling
       container_name: attacker
       priviliged: true
       networks:
         localnet:
           ipv4_address: 192.168.0.10
       tty: true
       stdin_open: true

     victim:
       image: kalilinux/kali-rolling
       container_name: victim
       priviliged: true
       networks:
         localnet:
           ipv4_address: 192.168.0.20
       tty: true
       stdin_open: true

     gateway:
       image: kalilinux/kali-rolling
       container_name: gateway
       priviliged: true
       networks:
         localnet:
           ipv4_address: 192.168.0.30
       tty: true
       stdin_open: true

   networks:
     localnet:
       driver: bridge
       ipam:
         config:
           - subnet: 192.168.0.0/24
   ```

   - Ce fichier définit trois machines dans le même réseau local (subnet `192.168.0.0/24`).

2. **Démarrage des conteneurs**
   - Dans le même répertoire que le fichier `docker-compose.yml`, exécutez la commande :
     ```bash
     docker-compose up -d
     ```

   - Vérifiez que les conteneurs sont bien en cours d'exécution avec :
     ```bash
     docker ps
     ```

### Partie 2 : Lancement de l'attaque ARP Spoofing

1. **Configurer les machines**
   - Connectez-vous à chaque machine pour les préparer à l'attaque.
   - Depuis votre terminal, exécutez les commandes suivantes pour accéder à chaque conteneur :
     - Pour l'attaquant :
       ```bash
       docker exec -it attacker /bin/bash
       ```
     - Pour la victime :
       ```bash
       docker exec -it victim /bin/bash
       ```
     - Pour la passerelle :
       ```bash
       docker exec -it gateway /bin/bash
       ```

2. **Installer les outils nécessaires**
   - Sur chaque machine, mettez à jour les paquets et installez **dsniff** (qui inclut arpspoof).
   - Exécutez cette commande **sur chaque machine** :
     ```bash
     apt-get update && apt-get install -y dsniff iputils-ping
     ```
   - Exécutez sur la machine **victim** :
     ```bash
     apt-get install net-tools
     ```

3. **Test de connectivité**
   - Vérifiez que la victime peut pinguer la passerelle

   - Vous devriez obtenir des réponses de la passerelle.
   
4. **Observer l'état de la table ARP de la victime**
	- Sur le conteneur **victim** vérifiez l'état de la table ARP (n'hésitez pas à utiliser le man).
    - Quel est l'adresse MAC de la passerelle et à quelle adresse IP est-elle associé ?

5. **Lancer l'attaque ARP Spoofing**
   - Depuis le conteneur **attacker**, exécutez la commande suivante pour lancer l'attaque ARP Spoofing :
     ```bash
     arpspoof -i eth0 -t [adresse de la victime] [adresse de la passerelle]
     ```
   - Cette commande trompe la victime en lui faisant croire que l'attaquant est la passerelle.

6. **Observer l'effet de l'attaque**
	- Sur le conteneur **victim** vérifiez l'état de la table ARP (n'hésitez pas à utiliser le man).

   - Quel est donc maintenant l'adresse MAC associé à l'adresse IP de la passerelle ?

### Partie 3 : Analyse et conclusion

1. **Impact sur le trafic**
   - Pinguez la passerelle depuis la victime pendant que l'attaque est en cours. Observez ce qu'il se passe au niveau des paquets.

2. **Proposez des solutions pour contrer cette attaque**

3. **Nettoyage**
   - Pour arrêter l'attaque et les conteneurs :
     ```bash
     docker-compose down
     ```
     
## Exercice 2

Configurer une table ARP statique permet de définir manuellement des associations entre des adresses IP et des adresses MAC, empêchant ainsi la modification dynamique de ces associations. Cela peut être utile pour prévenir des attaques telles que l'**ARP Spoofing**.

### Configuration d'une table ARP statique sous Linux

Tu peux utiliser la commande `arp` pour ajouter des entrées statiques à la table ARP. Voici les étapes pour configurer une table ARP statique sous Linux.

1. **Afficher la table ARP actuelle :**
   ```bash
   arp -n
   ```

2. **Ajouter une entrée statique à la table ARP :**
   La commande suivante lie une adresse IP à une adresse MAC de manière statique.
   ```bash
   sudo arp -s [adresse IP] [adresse MAC]
   ```

3. **Vérifier que l'entrée statique a bien était ajoutée à la table**

4. **Supprimer une entrée statique :**
   ```bash
   sudo arp -d [adresse IP]
   ```

>**⚠️ Warning**<br/>
>Les changements apportés ne sont pas persistants après un redémarrage. Pour rendre ces changements persistants, il est nécessaire d'ajouter ces commandes dans des scripts qui sont exécutés au démarrage (par exemple via `crontab` pour Linux).

## Exercice 3 - Bonus

Réécrivez l'attaque **arpspoof** en utilisant **python**. 
>**⚠️ Warning**<br/>
>Pour réaliser cet exercice, il vous faut installer : 
>- Un éditeur de texte comme nano ou VIM sur l'image de l'attaquant
>- Python3 et pip
>- La librairie scapy (`apt install python3-scapy`)
